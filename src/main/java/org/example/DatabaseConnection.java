package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {

    private static final Logger LOGGER = LogManager.getLogger(DatabaseConnection.class);
    private static String url;
    private static String user;
    private static String password;

    static {
        try (InputStream input =
                     DatabaseConnection.class.getClassLoader().getResourceAsStream("dbconfig.properties")) {
            Properties properties = new Properties();
            properties.load(input);
            url = properties.getProperty("db.url");
            user = properties.getProperty("db.user");
            password = properties.getProperty("db.password");
        } catch (IOException e) {
            LOGGER.error("Error reading the database configuration file: {}", e.getMessage(), e);
        }
    }

    private DatabaseConnection() {
    }

    public static Connection connect() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
            LOGGER.info("Connected to the database.");
        } catch (SQLException e) {
            LOGGER.error("Error connecting to the database: {}", e.getMessage(), e);
        }
        return connection;
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
                LOGGER.info("Connection closed.");
            } catch (SQLException e) {
                LOGGER.error("Error closing the connection: {}", e.getMessage(), e);
            }
        }
    }
}