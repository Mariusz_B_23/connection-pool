package org.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class AppLoop {

    private static final Logger LOGGER = LogManager.getLogger(AppLoop.class);

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++) {
            LOGGER.info("Iteration: {}", i + 1);
            Connection connection = null;
            Statement statement = null;
            ResultSet resultSet = null;


            try {
                connection = DatabaseConnection.connect();

                if (connection != null) {
                    statement = connection.createStatement();

                    String query = "SELECT * FROM users";
                    resultSet = statement.executeQuery(query);

                    while (resultSet.next()) {
                        int id = resultSet.getInt("id");
                        String username = resultSet.getString("username");
                        String password = resultSet.getString("password");
                        LOGGER.info("ID: {}, Username: {}, Password: {}", id, username, password);
                    }
                } else {
                    LOGGER.error("Failed to make connection!");
                }

            } catch (Exception e) {
                LOGGER.error("An error occurred", e);
            } finally {
                try {
                    if (resultSet != null) resultSet.close();
                    if (statement != null) statement.close();
                    DatabaseConnection.close(connection);
                } catch (Exception e) {
                    LOGGER.error("Error closing resources", e);
                }
            }
        }

        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        long minutes = (duration / 1000) / 60;
        long seconds = (duration / 1000) % 60;

        LOGGER.info("Total time taken for 10000 queries: {} minutes, {} seconds", minutes, seconds);
    }

}